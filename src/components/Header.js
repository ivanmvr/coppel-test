import React from 'react';
import Logo from './Logo';
import Button from '@material-ui/core/Button';

export default function Header() {
  const logoImg = "https://i7.pngguru.com/preview/362/866/527/web-development-world-wide-web-computer-icons-website-world-wide-web-icon-png-thumbnail.jpg";
  return (
    <div id="headerContainer">
      <Logo imgURL={logoImg}/>
      <div className="headerButtons">
        <Button variant="text" color="primary">Join</Button>
        <Button variant="contained" color="primary">Login</Button>
      </div>
    </div>
  );
}
