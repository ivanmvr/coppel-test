import React from 'react';

export default function Logo(props) {
  return (
    <div id="headerLogo">
      <img src={props.imgURL} alt="Logo"/>
    </div>
  );
}
