import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import CoppelApp from '../components/CoppelApp';

function AppRouter() {
  return(
    <BrowserRouter>
      <React.Fragment>
        <Switch>
          <Route path="/" component={CoppelApp} exact={true}/>
        </Switch>
      </React.Fragment>
    </BrowserRouter>
  );
}

export default AppRouter;
